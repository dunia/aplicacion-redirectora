import socket
import random

server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server_socket.bind(('', 1236))
server_socket.listen(5)
print(socket.gethostbyname(socket.gethostname()))


try:
    while True:
        print("Waiting connections...")
        client_socket, addr = server_socket.accept()
        print("HTTP request received")
        list_urls = ["https://www.aulavirtual.urjc.es",
                    "https://www.courir.es/",
                    "https://www.amazon.es/"]
        random_url = str(random.choice(list_urls))
        print(random_url)
        htmlBody = "<h1>HTTP/1.1 301 Moved Permanently</h1>" + '<p>El sitio web al que intenta acceder ya no tiene este dominio. Pulse en el link para ser redireccionado al nuevo dominio: <a href="' \
                   + random_url + '">' + random_url + "</a></p>"
        client_socket.send(b"HTTP/1.1 200 OK \r\n\r\n" +
                        b"<html><body>" + htmlBody.encode('ascii') + b"</body></html>" +
                        b"\r\n")
        client_socket.close()

except KeyboardInterrupt:
    print("Closing binded socket")
    server_socket.close()